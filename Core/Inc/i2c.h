/*
 * test.h
 *
 *  Created on: Jan 8, 2021
 *      Author: isee
 */

#ifndef INC_I2C_H_
#define INC_I2C_H_

#include "queue.h"

typedef void (*OnI2cMessageComplete) (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay);
typedef void (*OnI2cMessageError) (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize);

typedef struct t_i2c_msg {
	uint16_t addr;
	uint8_t *pData;
	uint16_t pSize;
	uint8_t *pRespData;
	uint16_t pRespSize;
	uint16_t smDelay;
	OnI2cMessageComplete evMessageComplete;
	OnI2cMessageError evMessageError;
} i2c_msg;


void i2c_initqueues(void);
bool queue_push (uint16_t addr, uint8_t *pData, uint16_t pSize, uint16_t wRespSize);
struct t_i2c_msg* queue_pop(void);
void free_message(struct t_i2c_msg* i);
void i2c_Timer(void);
bool i2c_getMessage (struct t_i2c_msg *message);
bool i2c_dropMessage();


/* public functions */
void i2c_SchedMessage (void);
bool i2c_pushMessage (uint16_t addr, uint8_t *pData, uint16_t pSize, uint16_t wRespSize, uint16_t smDelay, OnI2cMessageComplete evMC, OnI2cMessageError evME);


#endif /* INC_I2C_H_ */
