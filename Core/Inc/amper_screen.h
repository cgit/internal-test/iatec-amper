/*
 * amper_screen.h
 *
 *  Created on: Feb 18, 2021
 *      Author: isee
 */

#ifndef INC_AMPER_SCREEN_H_
#define INC_AMPER_SCREEN_H_

void amper_screen_init(void);

/* Public API */
void update_voltage (float reqVoltage);
void update_current (float reqCurrent);
void update_power (float reqPower);

void update_status (uint8_t nStatus);

void set_error(uint16_t nError);

#endif /* INC_AMPER_SCREEN_H_ */
