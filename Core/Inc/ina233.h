/*
 * ina233.h
 *
 *  Created on: Jan 18, 2021
 *      Author: isee
 */

#ifndef INC_INA233_H_
#define INC_INA233_H_

void ina233_schedule (void);

void ina233_timer (void);

#endif /* INC_INA233_H_ */
