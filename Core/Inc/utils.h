/*
 * utils.h
 *
 *  Created on: 21 may. 2019
 *      Author: mcaro
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>

int find (char* pData, char key, int idxpos);

uint8_t* toupper_str(uint8_t *pData);

const char* cut (char *pData, uint16_t from, uint16_t size);
const char* getKey (char *pData, char key);
const char* getValue (char* pData, char key);

const char* float2str (float x);
float stof (const char* s);
const char* f2str (float x, char* data, uint16_t sz, uint8_t nDec);
int ipow(int base, int exp);

#endif /* UTILS_H_ */
