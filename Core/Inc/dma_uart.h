/*
 * dma_uart.h
 *
 *  Created on: Feb 11, 2021
 *      Author: isee
 */

#ifndef INC_DMA_UART_H_
#define INC_DMA_UART_H_

#include "queue.h"

typedef struct t_uart_data {
	uint8_t *pData;
	uint16_t pSize;
} uart_data;

bool send_uart_message(uint8_t* dat, uint8_t datSz);
bool get_uart_message(struct t_uart_data *dat);
bool drop_uart_message();

/* private */
void uart_schedule (void);

#endif /* INC_DMA_UART_H_ */
