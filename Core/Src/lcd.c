/*
 * lcd.c
 *
 *  Created on: 01 Dic. 2020
 *      Author: isee
 */

#include "lcd.h"
#include "i2c.h"
#include <string.h>
#include <main.h>

uint8_t lcd_addr = LCD_ADDRESS;
uint8_t RGB_Addr = RGB_ADDRESS;
uint8_t _cols = 16;
uint8_t _rows = 2;
uint8_t dotsize = LCD_5x8DOTS;
uint8_t _showcontrol = 0;
uint8_t _showmode = 0;
uint8_t _showfunction = LCD_4BITMODE | LCD_1LINE | LCD_2LINE | LCD_5x8DOTS ;

const uint8_t color_define[4][3] =
{
    {255, 255, 255},            // white
    {255, 0, 0},                // red
    {0, 255, 0},                // green
    {0, 0, 255},                // blue
};

// uint32_t __sync_i2c_write (uint16_t addr, uint8_t* pData, uint16_t Size);
// uint32_t __sync_i2c_read (uint16_t addr, uint8_t* pData, uint16_t Size);

void OnLCDI2cMsgComplete (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	if(delay > 0)
		HAL_Delay(delay);
}

void OnLCDI2cMsgError (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
	__NOP();
}


void send (uint8_t *pData, uint8_t pSz, uint16_t delay)
{
	// __sync_i2c_write (lcd_addr , pData, Size);
	i2c_pushMessage(lcd_addr, pData, pSz, 0, delay, OnLCDI2cMsgComplete, OnLCDI2cMsgError);
}

void setReg(uint8_t addr, uint8_t data, uint16_t delay)
{
	uint8_t pData [] = { addr , data };
	// __sync_i2c_write (RGB_Addr , pData, sizeof(pData));
	i2c_pushMessage(RGB_Addr, pData, sizeof(pData), 0, delay, OnLCDI2cMsgComplete, OnLCDI2cMsgError);
}

void command (uint8_t value, uint16_t delay)
{
    uint8_t data[2] = {0x80, value};
    send(data, sizeof(data), delay);
}

void display()
{
    _showcontrol |= LCD_DISPLAYON;
    command(LCD_DISPLAYCONTROL | _showcontrol, 0);
}

void noDisplay()
{
    _showcontrol &= ~LCD_DISPLAYON;
    command(LCD_DISPLAYCONTROL | _showcontrol, 0);
}

void clear()
{
    command(LCD_CLEARDISPLAY, 2);        // clear display, set cursor position to zero
}

void setRGB(uint8_t r, uint8_t g, uint8_t b)
{
    setReg(REG_RED, r, 0);
    setReg(REG_GREEN, g, 0);
    setReg(REG_BLUE, b, 0);
}

void setColorWhite(){ setRGB(255, 255, 255); }


void stopBlink()
{
    _showcontrol &= ~LCD_BLINKON;
    command(LCD_DISPLAYCONTROL | _showcontrol, 0);
}

void blink()
{
    _showcontrol |= LCD_BLINKON;
    command(LCD_DISPLAYCONTROL | _showcontrol, 0);
}

void noCursor()
{
    _showcontrol &= ~LCD_CURSORON;
    command(LCD_DISPLAYCONTROL | _showcontrol, 0);
}

void cursor() {
    _showcontrol |= LCD_CURSORON;
    command(LCD_DISPLAYCONTROL | _showcontrol, 0);
}

void scrollDisplayLeft(void)
{
    command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT, 0);
}

void scrollDisplayRight(void)
{
    command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT, 0);
}

void leftToRight(void)
{
    _showmode |= LCD_ENTRYLEFT;
    command(LCD_ENTRYMODESET | _showmode, 0);
}

void rightToLeft(void)
{
    _showmode &= ~LCD_ENTRYLEFT;
    command(LCD_ENTRYMODESET | _showmode, 0);
}

void noAutoscroll(void)
{
    _showmode &= ~LCD_ENTRYSHIFTINCREMENT;
    command(LCD_ENTRYMODESET | _showmode, 0);
}

void autoscroll(void)
{
    _showmode |= LCD_ENTRYSHIFTINCREMENT;
    command(LCD_ENTRYMODESET | _showmode, 0);
}

void customSymbol(uint8_t location, uint8_t charmap[])
{

    location &= 0x7; // we only have 8 locations 0-7
    command(LCD_SETCGRAMADDR | (location << 3), 0);


    uint8_t data[9];
    data[0] = 0x40;
    for(int i=0; i<8; i++)
    {
        data[i+1] = charmap[i];
    }
    send(data, sizeof(data), 0);
}

void setCursor(uint8_t col, uint8_t row)
{
    col = (row == 0 ? col|0x80 : col|0xc0);
    uint8_t data[2] = {0x80, col};
    send(data, sizeof(data), 0);
}


void setColor(uint8_t color)
{
    if(color > 3)return ;
    setRGB(color_define[color][0], color_define[color][1], color_define[color][2]);
}

void blinkLED(void)
{
    ///< blink period in seconds = (<reg 7> + 1) / 24
    ///< on/off ratio = <reg 6> / 256
    setReg(0x07, 0x17, 0);  // blink every second
    setReg(0x06, 0x7f, 0);  // half on, half off
}

void noBlinkLED(void)
{
    setReg(0x07, 0x00, 0);
    setReg(0x06, 0xff, 0);
}

void write_char(uint8_t value)
{
    uint8_t data[2] = {0x40, value};
    send(data, sizeof(data), 0);
}

void lcd_write (char* data)
{
	uint8_t size = strlen(data);
	for(uint8_t i=0; i < size; i++){
		write_char(data[i]);
	}
}

void init_lcd (void)
{
	HAL_Delay(50);
	command( LCD_FUNCTIONSET | _showfunction, 5);
	// HAL_Delay(5);
	///< second try
    command(LCD_FUNCTIONSET | _showfunction, 5);
    // HAL_Delay(5);
    ///< third go
    command(LCD_FUNCTIONSET | _showfunction, 0);

    ///< turn the display on with no cursor or blinking default
    _showcontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
    display();
    clear();

    _showmode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
    ///< set the entry mode
    command(LCD_ENTRYMODESET | _showmode, 0);

    ///< backlight init
    setReg(REG_MODE1, 0, 0);
    ///< set LEDs controllable by both PWM and GRPPWM registers
    setReg(REG_OUTPUT, 0xFF, 0);
    ///< set MODE2 values
    ///< 0010 0000 -> 0x20  (DMBLNK to 1, ie blinky mode)
    setReg(REG_MODE2, 0x20, 0);

    setColorWhite();
}
