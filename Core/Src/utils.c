/*
 * utils.c
 *
 *  Created on: 21 may. 2019
 *      Author: mcaro
 */

#include <ctype.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include "utils.h"

/* locate char inside buffer, buffer must be '\0' terminated
 * return: pos or -1 if not found
 * */
int find (char* pData, char key, int idxpos)
{
	int idx = idxpos;
	while(pData[idx] != '\0'){
		if(pData[idx] == key)
			return idx;
		idx++;
	}
	if(key == '\0')
		return idx;
	return -1;
}

uint8_t* toupper_str(uint8_t *pData)
{
	char *s = (char*) pData; 			// Convert to upper case
	while (*s) {
		*s = toupper((unsigned char) *s);
		s++;
	}
	return pData;
}

#define CHAR_BUFF_SIZE	50
static char iData [CHAR_BUFF_SIZE];

const char* cut (char *pData, uint16_t from, uint16_t size)
{
	memset(iData, 0, 50);
	memcpy(iData, pData + from, size);
	return iData;
}

const char* getKey (char *pData, char key)
{
	int pos = find(pData, key, 0);
	if(pos == -1)
		return NULL;
	return cut(pData, 0, pos);
}

const char* getValue (char* pData, char key)
{
	int pos = find(pData, key, 0);
	if(pos == -1)
		return NULL;
	return cut(pData, pos + 1, strlen(pData) - pos);
}
#if 0
const char* ftos (float x, char* pBuff, uint16_t pBuffSize)
{
    char *s = pBuff + pBuffSize; // go to end of buffer
    uint16_t decimals;  // variable to store the decimals
    int units;  // variable to store the units (part to left of decimal place)
    if (x < 0) { // take care of negative numbers
        decimals = (int)(x * -100) % 100; // make 1000 for 3 decimals etc.
        units = (int)(-1 * x);
    } else { // positive numbers
        decimals = (int)(x * 100) % 100;
        units = (int)x;
    }

    *--s = (decimals % 10) + '0';
    decimals /= 10; // repeat for as many decimal places as you need
    *--s = (decimals % 10) + '0';
    *--s = '.';

    do{
        *--s = (units % 10) + '0';
        units /= 10;
    }while (units > 0);

#if 0
    while (units > 0) {
        *--s = (units % 10) + '0';
        units /= 10;
    }
#endif

    if (x < 0) *--s = '-'; // unary minus sign for negative numbers
    return s;
}
#endif

int ipow(int base, int exp)
{
    int result = 1;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}

const char* float2str (float x)
{
    char *s = iData + CHAR_BUFF_SIZE; // go to end of buffer
    uint16_t decimals;  // variable to store the decimals
    int units;  // variable to store the units (part to left of decimal place)
    if (x < 0) { // take care of negative numbers
        decimals = (int)(x * -100) % 100; // make 1000 for 3 decimals etc.
        units = (int)(-1 * x);
    } else { // positive numbers
        decimals = (int)(x * 100) % 100;
        units = (int)x;
    }

    *--s = (decimals % 10) + '0';
    decimals /= 10; // repeat for as many decimal places as you need
    *--s = (decimals % 10) + '0';
    *--s = '.';

    do{
        *--s = (units % 10) + '0';
        units /= 10;
    }while (units > 0);

    if (x < 0) *--s = '-'; // unary minus sign for negative numbers
    return s;
}

const char* f2str (float x, char* data, uint16_t sz, uint8_t nDec)
{
    char *s = data + sz; // go to end of buffer
    uint16_t decimals;  // variable to store the decimals
    int units;  // variable to store the units (part to left of decimal place)
    if (x < 0) { // take care of negative numbers
        decimals = (int)(x * -100) % 100; // make 1000 for 3 decimals etc.
        units = (int)(-1 * x);
    } else { // positive numbers
        decimals = (int) (x * ipow(10, nDec)) % ipow(10, nDec);
        units = (int)x;
    }

    while(nDec--)
    {
    	*--s = (decimals % 10) + '0';
    	decimals /= 10; // repeat for as many decimal places as you need
    }

    // *--s = (decimals % 10) + '0';
    *--s = '.';

    do{
        *--s = (units % 10) + '0';
        units /= 10;
    }while (units > 0);

    if (x < 0) *--s = '-'; // unary minus sign for negative numbers
    return s;
}


float stof (const char* s)
{
  float rez = 0, fact = 1;
  if (*s == '-'){
    s++;
    fact = -1;
  };
  for (int point_seen = 0; *s; s++){
    if (*s == '.'){
      point_seen = 1;
      continue;
    };
    int d = *s - '0';
    if (d >= 0 && d <= 9){
      if (point_seen) fact /= 10.0f;
      rez = rez * 10.0f + (float)d;
    };
  };
  return rez * fact;
};

