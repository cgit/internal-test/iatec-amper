/*
 * amper_screen.c
 *
 *  Created on: Feb 18, 2021
 *      Author: isee
 */
#include "main.h"
#include "amper_screen.h"
#include "utils.h"
#include "lcd.h"
#include <string.h>
#include <math.h>

float s_voltage = 0.0;
float s_power = 0.0;
float s_current = 0.0;
uint8_t s_status = 0;

uint16_t s_error = 0;

char lcdData [16 * 2];

void update_voltage (float reqVoltage)
{
	char dat[20];
	float nearest = ceilf(reqVoltage * 1000) / 1000;
	if(nearest != s_voltage){
		s_voltage = nearest;
		setCursor(0, 0);
		sprintf(lcdData, "V:%sV", f2str(s_voltage, dat, 10, 3));
		lcd_write(lcdData);
	}
}

void update_current (float reqCurrent)
{
	char dat[20];
	float nearest = ceilf(reqCurrent * 1000) / 1000;
	if(nearest != s_current){
		s_current = nearest;
		setCursor(0, 1);
		sprintf(lcdData, "I:%sA", f2str(s_current, dat, 10, 3));
		lcd_write(lcdData);
	}
}

void update_power (float reqPower)
{
	char dat[20];
	float nearest = ceilf(reqPower * 100) / 100;
	if(nearest != s_power){
		s_power = nearest;
		setCursor(9, 0);
		sprintf(lcdData, "P:%sW", f2str(s_power, dat, 10, 2));
		lcd_write(lcdData);
	}
}

void set_error(uint16_t nError)
{

}

#define STATUS_MASK		0x62

void update_status (uint8_t nStatus)
{
	nStatus = nStatus & STATUS_MASK;
	if(nStatus != s_status){
		s_status = nStatus;
		setCursor(9, 1);
		sprintf(lcdData, "E:0x%.2x", s_status);
		lcd_write(lcdData);
	}
}

void amper_screen_init(void)
{
	memset(lcdData, 0, 16*2);
	clear();
	// Write Voltage at pos (0,0)
	sprintf(lcdData, "V:%sV ", float2str(s_voltage));
	lcd_write(lcdData);
	// Write Power at pos (8,0)
	setCursor(9, 0);
	sprintf(lcdData, "P:%sW", float2str(s_power));
	lcd_write(lcdData);
	// Write Power at pos (0,1)
	setCursor(0, 1);
	sprintf(lcdData, "I:%sA", float2str(s_current));
	lcd_write(lcdData);
	setCursor(9, 1);
	sprintf(lcdData, "E:0x%x", s_error);
	lcd_write(lcdData);
}

