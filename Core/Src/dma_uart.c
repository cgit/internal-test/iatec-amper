/*
 * dma_uart.c
 *
 *  Created on: Feb 10, 2021
 *      Author: isee
 */

#include "main.h"
#include "dma_uart.h"
#include <string.h>
#include <stdlib.h>

enum t_dmauart_state {
	uartINIT,
	uartCheckTxPending,
	uart_WAIT_TXComplete,
};

enum t_dmauart_state dma_uart_state = uartINIT;

#define MAX_COMMAND_SZ	16

extern UART_HandleTypeDef huart2;
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;

#define RX_DATABUFFER_SZ	16
uint8_t dma_rx_buffer[RX_DATABUFFER_SZ];
uint8_t dma_rx_Count = 0;

uint8_t dma_rx_buffer2[RX_DATABUFFER_SZ];
uint8_t dma_rx_Count2 = 0;

uint8_t *dma_rx_ActualpBuffer = NULL;
uint8_t *dma_rx_ActualpCount = NULL;
uint8_t *dma_rx_PreviouspBuffer = NULL;
uint8_t *dma_rx_PreviouspCount = NULL;

#define QUART_RX_QUEUE_SIZE		64
Queue_t q_uartRXMessages;
Queue_t q_uartTXMessages;

bool OnTransmit = false;

#define DMA_RX_DATABUFFER_SZ	128
uint8_t dma_circular_rx_dbuffer [DMA_RX_DATABUFFER_SZ];
uint16_t dma_rx_p = 0;
uint16_t dma_wr_p = 0;

void uart_init(void)
{
	memset(dma_rx_buffer, 0, RX_DATABUFFER_SZ);
	memset(dma_rx_buffer2, 0, RX_DATABUFFER_SZ);
	dma_rx_Count = 0;
	dma_rx_Count2 = 0;
	dma_rx_ActualpBuffer = dma_rx_buffer;
	dma_rx_ActualpCount = &dma_rx_Count;
	dma_rx_PreviouspBuffer = dma_rx_buffer2;
	dma_rx_PreviouspCount = &dma_rx_Count2;
	q_init(&q_uartRXMessages, sizeof(struct t_uart_data), QUART_RX_QUEUE_SIZE, FIFO, false);
	q_init(&q_uartTXMessages, sizeof(struct t_uart_data), QUART_RX_QUEUE_SIZE, FIFO, false);
	HAL_UART_Receive_DMA(&huart2, dma_rx_ActualpBuffer , RX_DATABUFFER_SZ);
}

void swapBuffer(void)
{
	if(dma_rx_ActualpBuffer == dma_rx_buffer){
		dma_rx_ActualpBuffer = dma_rx_buffer2;
		dma_rx_ActualpCount = &dma_rx_Count2;
		dma_rx_PreviouspBuffer = dma_rx_buffer;
		dma_rx_PreviouspCount = &dma_rx_Count;
	}
	else{
		dma_rx_ActualpBuffer = dma_rx_buffer;
		dma_rx_ActualpCount = &dma_rx_Count;
		dma_rx_PreviouspBuffer = dma_rx_buffer2;
		dma_rx_PreviouspCount = &dma_rx_Count2;
	}
}

// uint8_t dma_circular_rx_dbuffer [DMA_RX_DATABUFFER_SZ];
// uint16_t dma_rx_p = 0;
// uint16_t dma_wr_p = 0;

void add_data_to_rx_buffer (uint8_t *data, uint8_t sz)
{
	uint8_t count = 0;
	while(count < sz){
		if(dma_wr_p == dma_rx_p){
			dma_circular_rx_dbuffer[dma_wr_p++] = data[count++];
		}
		else if(dma_wr_p > dma_rx_p) {
			if((dma_wr_p + 1) >= DMA_RX_DATABUFFER_SZ){
				if(dma_rx_p == 0){
					count = sz;
					break;
				}
				else{
					// Start Again
					dma_wr_p = 0;
					dma_circular_rx_dbuffer[dma_wr_p++] = data[count++];
				}
			}
			else{
				dma_circular_rx_dbuffer[dma_wr_p++] = data[count++];
			}
		}
		else if(dma_wr_p < dma_rx_p) {
			if((dma_wr_p +1) == dma_rx_p){
				count = sz;
				break;
			}
			else{
				dma_circular_rx_dbuffer[dma_wr_p++] = data[count++];
			}
		}
	}
}

uint16_t get_data (uint8_t* data, uint16_t max_size)
{
	uint16_t count = 0;
	while(count <= max_size){
		if(dma_wr_p == dma_rx_p)
			return count;
		else if(dma_wr_p > dma_rx_p){
			data[count++] = dma_circular_rx_dbuffer[dma_rx_p++];
		}
		else if(dma_wr_p < dma_rx_p){
			if(dma_rx_p + 1 >= DMA_RX_DATABUFFER_SZ){
				if(dma_wr_p == 0){
					return count;
				}
				else{
					dma_rx_p = 0;
					data[count++] = dma_circular_rx_dbuffer[dma_rx_p++];
				}
			}
			else
				data[count++] = dma_circular_rx_dbuffer[dma_rx_p++];
		}
	}
	return count;
}

void USAR_UART_IDLECallback(UART_HandleTypeDef *huart)
{

	//Stop this DMA transmission
    HAL_UART_DMAStop(huart);
    //Calculate the length of the received data
    *dma_rx_ActualpCount  = RX_DATABUFFER_SZ - __HAL_DMA_GET_COUNTER(&hdma_usart2_rx);
    // Change Buffer
    swapBuffer();
    // Continue Receiving in new buffer
    HAL_UART_Receive_DMA(huart, dma_rx_ActualpBuffer, RX_DATABUFFER_SZ);
    // new data and new data Count resides in: dma_rx_PreviouspBuffer and *dma_rx_PreviouspCount

    add_data_to_rx_buffer(dma_rx_PreviouspBuffer, *dma_rx_PreviouspCount);

}

void USER_UART_IRQHandler(UART_HandleTypeDef *huart)
{
    if(USART2 == huart->Instance)                                   //Determine whether it is serial port 1
    {
        if(RESET != __HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE))   	//Judging whether it is idle interruption
        {
            __HAL_UART_CLEAR_IDLEFLAG(huart);                     	//Clear idle interrupt sign (otherwise it will continue to enter interrupt)
            USAR_UART_IDLECallback(huart);                          //Call interrupt handler
        }
    }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	OnTransmit = false;
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	__NOP();
}

uint8_t tmpbuffer [DMA_RX_DATABUFFER_SZ];
uint16_t tmpCount = 0;
uint8_t tmpMsg [DMA_RX_DATABUFFER_SZ];
uint16_t tmpMsgCount = 0;

void schedule_uart_lowlevel (void)
{
	struct t_uart_data msg;
	uint16_t data_count = get_data(tmpbuffer, DMA_RX_DATABUFFER_SZ);
	uint16_t count = 0;
	while(count < data_count){
		if(tmpbuffer[count] == '\n' || tmpbuffer[count] == '\r'){
			if(tmpMsgCount > 0){
				msg.pData = malloc(tmpMsgCount + 1);
				memcpy(msg.pData, tmpMsg, tmpMsgCount);
				msg.pSize = tmpMsgCount;
				msg.pData[tmpMsgCount] = '\0';
				q_push(&q_uartRXMessages, &msg);
				tmpMsgCount = 0;
			}
		}
		else{
			if((tmpMsgCount + 1) < DMA_RX_DATABUFFER_SZ)
				tmpMsg[tmpMsgCount++] = tmpbuffer[count];
			else{
				tmpMsgCount = 0;
			}
		}
		count++;
	}
}

bool send_uart_message(uint8_t* dat, uint8_t datSz)
{
	struct t_uart_data i;
	i.pData = malloc(datSz);
	i.pSize = datSz;
	memcpy(i.pData, dat, datSz);
	if(!q_push(&q_uartTXMessages, &i)){
		free(i.pData);
		return false;
	}
	return true;
}

bool get_uart_message(struct t_uart_data *dat)
{
	return q_peek(&q_uartRXMessages, dat);
}

bool drop_uart_message()
{
	struct t_uart_data dat;
	bool res = q_pop(&q_uartRXMessages, &dat);
	if(res)
		free(dat.pData);
	return res;
}

void uart_schedule (void)
{
	struct t_uart_data dat;
	bool res;

	if(dma_uart_state != uartINIT)
		schedule_uart_lowlevel();

	switch(dma_uart_state){
		case uartINIT:
			uart_init();
			dma_uart_state = uartCheckTxPending;
			break;
		case uartCheckTxPending:
			if(q_peek(&q_uartTXMessages, &dat)){
				OnTransmit = true;
				dma_uart_state = uart_WAIT_TXComplete;
				HAL_UART_Transmit_DMA(&huart2, dat.pData , dat.pSize);
			}
			break;
		case uart_WAIT_TXComplete:
			if(!OnTransmit){
				res = q_pop(&q_uartTXMessages, &dat);
				if(res)
					free(dat.pData);
				dma_uart_state = uartCheckTxPending;
			}
			break;
	}
}
