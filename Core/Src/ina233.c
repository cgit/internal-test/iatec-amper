/*
 * ina233.c
 *
 *  Created on: Jan 18, 2021
 *      Author: isee
 */

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "main.h"
#include "i2c.h"
#include "utils.h"
#include "ina233.h"
#include "dma_uart.h"
// #include "lcd.h"
#include "amper_screen.h"

extern TIM_HandleTypeDef htim6;

enum t_ina233_state {
	ina233INIT,
	ina233_WAIT_INIT_COMPLETE,
	ina233_WAIT4COMMAND,
	ina233_Measure_Voltage,
	ina233_Measure_Power,
	ina233_Measure_Current,
	ina233_Status,
};

enum t_ina233_state ina233_state = ina233INIT;

void enable_ina233_timer(uint16_t ms_interval);
void disable_ina233_timer(void);
bool isTimerExpired (void);


// ina233 slave address (A1 = 0 and A0 = 0) => 1000000xb (0x80 w or 0x81)

const char rOK[] = "OK\r\n";
const char rERROR[] = "ERROR\r\n";
const char rSTR[] = "%s\r\n";
const char rHexSTR[] = "0x%s\r\n";
const char rVERSION[] = "ISEE Amper v1.00\r\n";
const char rOK_DAT[] = "OK=0x%s\r\n";
const char rPOWER_OK_DAT[] = "0x%u\r\n";
const char rNEWLINE [] = "\r\n";
/*const char rBAKSPACE [] = "\b";*/

enum INA233_COMMANDS_REG {
	CMD_CLEAR_FAULTS = 0x03,
	CMD_RESTORE_DEFAULT = 0x12,
	CMD_GET_CAPABILITY = 0x19,
	CMD_GET_STATUS_BYTE = 0x78,
	CMD_GET_STATUS_WORD = 0x79,
	CMD_GET_IOUT_W_LIMIT = 0x4A,
	CMD_GET_VIN_OV_W_LIM = 0x57,
	CMD_GET_VIN_UV_W_LIM = 0x58,
	CMD_GET_PIN_OP_W_LIM = 0x6b,
	CMD_GET_STATUS_IOUT = 0x7b,
	CMD_GET_STATUS_INPUT = 0x7c,
	CMD_GET_STATUS_CML = 0x7e,
	CMD_GET_STATUS_MFR_SP = 0x80,
	CMD_GET_READ_EIN = 0x86,
	CMD_GET_READ_VIN = 0x88, /* here */
	CMD_GET_READ_IN = 0x89,
	CMD_GET_READ_VOUT = 0x8b,
	CMD_GET_READ_IOUT = 0x8c,
	CMD_GET_READ_POUT = 0x96,
	CMD_GET_READ_PIN = 0x97,
	CMD_GET_MFR_ID = 0x99,
	CMD_GET_MFR_MOD = 0x9a,
	CMD_GET_MFR_REV = 0x9b,
	CMD_GET_MFR_ADC_CONFIG = 0xd0,
	CMD_GET_MFR_READ_VSHUNT = 0xd1,
	CMD_GET_MFR_ALERT_MASK = 0xd2,
	CMD_GET_MFR_CALIBRATION = 0xd4,
	CMD_GET_MFR_DEV_CONFIG = 0xd5,
	CMD_GET_CLEAR_EIN = 0xd6,
	/* INFO */
	CMD_GET_AT = 0x30,
	CMD_GET_VER = 0x31,
	CMD_GET_HELP = 0x32,
	CMD_CLEAR_UART = 0x33,
	CMD_SET_CFG	= 0x34,
	CMD_GET_CFG	= 0x35,
	CMD_SET_RESET = 0x36,
	/* No Command */
	CMD_NOTHING = 0xff,
};

uint8_t reshex = 0;
extern uint8_t echo_uart;	// default 0

#define R_SHUNT 			0.020
#define MAX_CURRENT_A		2.000
#define IOUT_OC_W_LIM 		1.5
#define POUT_OP_W_LIM 		0 		//Definir un valor de pot�ncia m�xim
#define VIN_UV_W_LIM		0.3
#define VIN_OV_W_LIM		5.5
#define V_SLOPE				8 * pow(10, 2)	// V_SLOPE for write

#define ADC_SAMPLES_1		0
#define ADC_SAMPLES_4		1
#define ADC_SAMPLES_16		2
#define ADC_SAMPLES_64		3
#define ADC_SAMPLES_128		4
#define ADC_SAMPLES_256		5
#define ADC_SAMPLES_512		6
#define ADC_SAMPLES_1024	7

#define VBUSCT_140u			0
#define VBUSCT_204u			1
#define VBUSCT_332u			2
#define VBUSCT_588u			3
#define VBUSCT_1_1m			4
#define VBUSCT_2_1m			5
#define VBUSCT_4_1m			6
#define VBUSCT_8_2m			7

#define VSHCT_140u			0
#define VSHCT_204u			1
#define VSHCT_332u			2
#define VSHCT_588u			3
#define VSHCT_1_1m			4
#define VSHCT_2_1m			5
#define VSHCT_4_1m			6
#define VSHCT_8_2m			7

#define MODE_PDOWN			0		// Power-down (or shutdown)
#define MODE_SVT			1		// Shunt voltage, triggered
#define MODE_BVT			2		// Bus voltage, triggered
#define MODE_SBT			3		// Shunt and bus, triggered
#define MODE_PDOWN2			4		// Power-down (or shutdown)
#define MODE_SVC			5		// Shunt voltage, continuous
#define MODE_BVC			6		// Bus voltage, continuous
#define MODE_SBC			7		// Shunt and bus, continuous

#define CURRENT_LSB			(float) (MAX_CURRENT_A / pow(2, 15))

uint16_t ina233_calibration = round((float) (0.00512 / ((MAX_CURRENT_A / pow(2, 15)) * R_SHUNT)));
uint16_t ina233_adc_config = (ADC_SAMPLES_64 << 9) | (VBUSCT_2_1m << 6) | (VSHCT_2_1m << 3) | MODE_SBC;

float cv_r = (1.0/8.0) * 0.01;
float ci_r = (float) 1 / CURRENT_LSB;

uint16_t ina233_iout_oc_w_lim = (1/(MAX_CURRENT_A / pow(2, 15))) * IOUT_OC_W_LIM; 						//Over current
uint16_t ina233_vin_uv_w_lim = V_SLOPE * VIN_UV_W_LIM; 													//Under voltage
uint16_t ina233_pout_op_w_lim = (1/(MAX_CURRENT_A / pow(2, 15))) * POUT_OP_W_LIM;						//Output power
uint16_t ina233_vin_ov_w_lim = V_SLOPE * VIN_OV_W_LIM;													//Over voltage

enum INA233_CMD_TYPE { CMD_T_COMMAND, CMD_T_GET, CMD_T_SET, CMD_T_INFO, CMD_T_GET_FLOAT, CMD_T_SET_FLOAT };

struct t_INA233_COMMANDS;

/* functions defined as parsers */
void AT_Received(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize);
void get_voltage(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize);
void get_current(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize);
void get_power(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize);
void get_status(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize);
void clear_status(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize);

typedef uint8_t* (*T_PARSE_FUNC) (uint16_t , uint8_t*, uint16_t);
typedef float (*T_PARSE_FUNC_CALC) (float);

typedef void (*T_PARSE) (struct t_INA233_COMMANDS *cmd, uint8_t* pData, uint16_t pSize);

typedef struct t_INA233_COMMANDS {
	enum INA233_COMMANDS_REG reg;	// INA Register
	enum INA233_CMD_TYPE c_type;	// INA Command type
	const char *cmd_str;
	T_PARSE myFunc;
} INA233_COMMANDS ;

struct t_INA233_COMMANDS ina233_cmd_list [] = {
		{CMD_CLEAR_FAULTS, CMD_T_COMMAND, "AT+CLRFAULT", NULL},
		{CMD_NOTHING, CMD_T_COMMAND, "AT", AT_Received},
		{CMD_NOTHING, CMD_T_GET_FLOAT, "AT+VIN?", get_voltage},
		{CMD_NOTHING, CMD_T_GET_FLOAT, "AT+IN?", get_current},
		{CMD_NOTHING, CMD_T_GET_FLOAT, "AT+PIN?", get_power},
		{CMD_GET_STATUS_INPUT, CMD_T_GET, "AT+STATUS?", get_status},
		{CMD_NOTHING, CMD_T_GET, "AT+CLEAR", clear_status},
		{0, 0, 0, 0},
};

#if 0

uint8_t com_buffer [30];	// String end with '\0'
uint16_t count_data = 0;	// Count Bytes inside buffer
uint8_t resp_buffer[30];

typedef uint8_t* (*T_PARSE_FUNC) (uint16_t , uint8_t*, uint16_t);
typedef float (*T_PARSE_FUNC_CALC) (float);

uint8_t* power_calc (uint16_t, uint8_t*, uint16_t);
uint8_t* setcfg (uint16_t idx, uint8_t* pData, uint16_t pSize);
uint8_t* getcfg (uint16_t idx, uint8_t* pData, uint16_t pSize);

void reset (uint16_t idx, uint8_t* pData, uint16_t pSize);

float calc_voltage (float v);
float calc_current (float v);
float warn_current (float v);
float warn_voltage (float v);

enum INA233_CMD_TYPE { CMD_T_COMMAND, CMD_T_GET, CMD_T_SET, CMD_T_INFO, CMD_T_GET_FLOAT, CMD_T_SET_FLOAT };

struct INA233_COMMANDS_t {
	enum INA233_COMMANDS_REG reg;
	enum INA233_CMD_TYPE c_type;
	uint8_t nBytes_rd;
	uint8_t nBytes_wr;
	const char *cmd_str;
	const char *resp;
	T_PARSE_FUNC myFunc;
};

struct INA233_COMMANDS_t ina233_cmd_list [] = {
		{CMD_SET_RESET , CMD_T_INFO, 0, 0, "AT+RESET", rOK, reset},
		{CMD_CLEAR_FAULTS , CMD_T_COMMAND, 0, 0, "AT+CLRFAULT", rOK, NULL},
		{CMD_RESTORE_DEFAULT , CMD_T_COMMAND, 0, 0, "AT+RESTORE", rOK, NULL},
	//	{CMD_GET_CAPABILITY , CMD_T_GET, 1, 0, "AT+CAP?", rSTR, NULL},
		{CMD_GET_STATUS_BYTE , CMD_T_GET, 1, 0, "AT+STB?", rSTR, NULL},
		{CMD_GET_STATUS_WORD , CMD_T_GET, 2, 0, "AT+STW?", rSTR, NULL},
		{CMD_GET_AT , CMD_T_INFO, 0, 0, "AT", rOK, NULL},
		{CMD_GET_IOUT_W_LIMIT , CMD_T_SET_FLOAT, 0, 2, "AT+IOUT_W_LIM=%u", rOK, (T_PARSE_FUNC) warn_current},
		{CMD_GET_IOUT_W_LIMIT , CMD_T_GET_FLOAT,2, 0, "AT+IOUT_W_LIM?", rSTR, (T_PARSE_FUNC) calc_current},
		{CMD_GET_VIN_OV_W_LIM, CMD_T_SET, 0, 2, "AT+VIN_OV_W_LIM=%u", rOK_DAT, NULL},
		{CMD_GET_VIN_OV_W_LIM, CMD_T_GET, 2, 0, "AT+VIN_OV_W_LIM?", rSTR, NULL},
		{CMD_GET_VIN_UV_W_LIM, CMD_T_SET_FLOAT, 0, 2, "AT+VIN_UV_W_LIM=%u", rOK, (T_PARSE_FUNC) warn_voltage},
		{CMD_GET_VIN_UV_W_LIM, CMD_T_GET_FLOAT, 2, 0, "AT+VIN_UV_W_LIM?", rSTR,(T_PARSE_FUNC) calc_voltage},
		{CMD_GET_PIN_OP_W_LIM, CMD_T_SET, 0, 2, "AT+PIN_OP_W_LIM=%u", rOK_DAT, NULL},
		{CMD_GET_PIN_OP_W_LIM, CMD_T_GET,  2, 0, "AT+PIN_OP_W_LIM?", rSTR, NULL},
		/*{CMD_GET_STATUS_IOUT, CMD_T_SET, 0, 1, "AT+ST_IOUT=%u", rOK_DAT, NULL},
		{CMD_GET_STATUS_IOUT, CMD_T_GET, 1, 0, "AT+ST_IOUT?", rSTR, NULL},
		{CMD_GET_STATUS_INPUT, CMD_T_SET, 0, 1, "AT+ST_IN=%u", rOK_DAT, NULL},
		{CMD_GET_STATUS_INPUT, CMD_T_GET, 1, 0, "AT+ST_IN?", rSTR, NULL},
		{CMD_GET_STATUS_CML, CMD_T_SET, 0, 1, "AT+ST_COM=%u", rOK_DAT, NULL},
		{CMD_GET_STATUS_CML, CMD_T_GET, 1, 0, "AT+ST_COM?", rSTR, NULL},*/
		{CMD_GET_STATUS_MFR_SP, CMD_T_SET, 0, 1, "AT+ST_MFR=%u", rOK_DAT, NULL},
		{CMD_GET_STATUS_MFR_SP, CMD_T_GET, 1, 0, "AT+ST_MFR?", rSTR, NULL},
		{CMD_GET_READ_EIN, CMD_T_GET, 6, 0, "AT+REIN?", rPOWER_OK_DAT, power_calc},
		{CMD_GET_READ_VIN, CMD_T_GET_FLOAT, 2, 0, "AT+VIN?", rSTR, (T_PARSE_FUNC) calc_voltage},
		{CMD_GET_READ_IN, CMD_T_GET_FLOAT, 2, 0, "AT+IN?", rSTR, (T_PARSE_FUNC) calc_current},
		/*{CMD_GET_READ_VOUT, CMD_T_GET, 2, 0, "AT+VOUT?", rSTR, NULL},
		{CMD_GET_READ_IOUT, CMD_T_GET, 2, 0, "AT+IOUT?", rSTR, NULL},
		{CMD_GET_READ_POUT, CMD_T_GET, 2, 0, "AT+POUT?", rSTR, NULL},
		{CMD_GET_READ_PIN, CMD_T_GET, 2, 0, "AT+PIN?", rSTR, NULL},
		{CMD_GET_MFR_MOD, CMD_T_GET, 6, 0, "AT+MFR_MOD?", rSTR, NULL},*/
		{CMD_GET_MFR_ADC_CONFIG, CMD_T_GET, 2, 0, "AT+ADC?", rSTR, NULL},
		{CMD_GET_MFR_ADC_CONFIG, CMD_T_SET, 0, 2, "AT+ADC=%d", rOK_DAT, NULL},
		{CMD_GET_MFR_READ_VSHUNT, CMD_T_GET, 2, 0, "AT+VSHUNT?", rSTR, NULL},
		{CMD_GET_MFR_READ_VSHUNT, CMD_T_GET, 2, 0, "AT+AV?", rSTR, NULL},
		{CMD_GET_MFR_ALERT_MASK, CMD_T_GET, 1, 0, "AT+ALERT?", rSTR, NULL},
		{CMD_GET_MFR_ALERT_MASK, CMD_T_SET, 0, 1, "AT+ALERT=%u", rSTR, NULL},
		{CMD_GET_MFR_CALIBRATION, CMD_T_SET, 0, 2, "AT+CAL=%u", rOK_DAT, NULL},
		{CMD_GET_MFR_CALIBRATION, CMD_T_GET, 2, 0, "AT+CAL?", rSTR, NULL},
		/*{CMD_GET_MFR_DEV_CONFIG, CMD_T_SET, 0, 1, "AT+DEV=%u", rOK_DAT, NULL},
		{CMD_GET_MFR_DEV_CONFIG, CMD_T_GET, 1, 0, "AT+DEV?", rSTR, NULL},
		{CMD_GET_CLEAR_EIN, CMD_T_COMMAND, 0, 0, "AT+CLEAR_EIN?", rOK, NULL},*/
		{CMD_SET_CFG, CMD_T_INFO, 0, 0, "AT+SETCFG=%u,%u", rOK, setcfg},
		{CMD_GET_CFG, CMD_T_INFO, 0, 0, "AT+GETCFG=%u", rOK, getcfg},
		{CMD_GET_CFG, CMD_T_INFO, 0, 0, "AT+VER?", rVERSION, NULL},
		{0 , 0, 0, 0, 0, 0 , 0},
};


uint8_t* power_calc (uint16_t idx, uint8_t* pData, uint16_t pSize)
{
	uint16_t power_w = 0;
	memcpy(&power_w, &pData[1], 2);
	snprintf(resp_buffer, 30, ina233_cmd_list[idx].resp, power_w);
	return resp_buffer;
}

uint8_t* setcfg (uint16_t idx, uint8_t* pData, uint16_t pSize)
{
	int pos = find((char*) pData, '=', 0);
	if(pos == -1)
		return (uint8_t*) rERROR;
	int key = find((char*) pData, ',', pos + 1);
	memset(resp_buffer, 0, 30);
	memcpy(resp_buffer, pData + pos + 1, (key - pos) - 1);
	int valpos = strlen(pData + key + 1);
	memcpy(pData, pData + key + 1, valpos + 1);
	if(!strcmp(resp_buffer, "RESHEX")){
		reshex = (uint8_t) atoi((const char*) pData);
	}
	else if(!strcmp(resp_buffer, "ECHO")){
		echo_uart = (uint8_t) atoi((const char*) pData);
	}
	return (uint8_t*) rOK;
}

uint8_t* getcfg (uint16_t idx, uint8_t* pData, uint16_t pSize)
{
	int pos = find((char*) pData, '=', 0);
	if(pos == -1)
		return (uint8_t*) rERROR;
	pData = pData + (pos + 1);
	if(!strcmp(pData, "RESHEX")){
		snprintf(resp_buffer, 30, "reshex=%u\r\n", reshex);
	}
	else if(!strcmp(pData, "ECHO")){
		snprintf(resp_buffer, 30, "echo=%u\r\n", echo_uart);
	}
	return resp_buffer;
}

void reset (uint16_t idx, uint8_t* pData, uint16_t pSize)
{
	HAL_NVIC_SystemReset();
}

float calc_voltage (float v)
{
	return cv_r * v;
}

float calc_current (float v)
{
	return v / ci_r;
}

float warn_current (float v)
{
	return v * ci_r;
}

float warn_voltage (float v)
{
	return v / cv_r;
}

void configure_ina233(void)
{
	/* Clear defaults */
	ina233_send_command(CMD_CLEAR_FAULTS);

    /* Set CONFIGURATION ADC to 64 samples; Ts = 2ms; Continuous Mode */
    /* AT+ADC=18287 [0x476f] -> 0x76f */
	// ina233_write_word(CMD_GET_MFR_ADC_CONFIG , ina233_adc_config);
	ina233_write_buffer(CMD_GET_MFR_ADC_CONFIG, (uint8_t*) &ina233_adc_config, sizeof(ina233_adc_config));

	/* Set CALIBRATION to m = 16384 and CLSB to 61 uA */
    /* AT+CAL=4194 */
	// ina233_write_word(CMD_GET_MFR_CALIBRATION, ina233_calibration);
	ina233_write_buffer(CMD_GET_MFR_CALIBRATION, (uint8_t*) &ina233_calibration, sizeof(ina233_calibration));

	/* Set OVERCURRENT alert limit to 1'5 A -> 24576 */
	//Write --> Y = 16384 x X
	/* AT+IOUT_W_LIM?*/ /*16384 [0x4000]*/
	//OVERCURRENT
	// ina233_write_word(0x4A,ina233_iout_oc_w_lim);
	ina233_write_buffer(CMD_GET_IOUT_W_LIMIT, (uint8_t*) &ina233_iout_oc_w_lim, sizeof(ina233_iout_oc_w_lim));

	//UNDERVOLTAGE
	/* LSB = 1.25mV -> X = 800; 800X3 = 2400 -> 3V  */
	// ina233_write_word(0x58,ina233_vin_uv_w_lim);
	ina233_write_buffer(CMD_GET_VIN_UV_W_LIM, (uint8_t*) &ina233_vin_uv_w_lim, sizeof(ina233_vin_uv_w_lim));

	//Output OVERPOWER warn limit threshold
	//AT+PIN_OP_W_LIM?
	// ina233_write_word(0x6B,ina233_pout_op_w_lim);
	ina233_write_buffer(CMD_GET_PIN_OP_W_LIM, (uint8_t*) &ina233_pout_op_w_lim, sizeof(ina233_pout_op_w_lim));

	//OVERVOLTAGE
	/* LSB = 1.25mV -> X = 800; */
	// ina233_write_word(0x57,ina233_vin_ov_w_lim);
	ina233_write_buffer(CMD_GET_VIN_OV_W_LIM, (uint8_t*) &ina233_vin_ov_w_lim, sizeof(ina233_vin_ov_w_lim));
}

const uint8_t* parse_command (uint8_t* pData, uint16_t bSize)
{
	int FExit = 0;			// Flag to End parse
	uint16_t index;
	char w[10];
	uint16_t bRead = 0; //uint16_t bRead = 0;
	uint16_t bWrite = 0;
	uint16_t idx = 0;	// locate command parse
	float wdata;
	const uint8_t* bData = NULL;

	/* Convert str data to UPPER*/
	pData = toupper_str (pData);

	while((ina233_cmd_list[idx].reg != 0) &&( FExit == 0)){	//El valor de idx se va actualizando hasta encontrar el comando que le toca.
		switch(ina233_cmd_list[idx].c_type){
		case CMD_T_COMMAND:
			if(!strcmp( ina233_cmd_list[idx].cmd_str , (char*) pData)){
				if(!ina233_send_command(ina233_cmd_list[idx].reg))
					bData = rERROR;
				else
					bData = ina233_cmd_list[idx].resp;
				FExit = 1;
			}
			break;
		case CMD_T_GET:
			if(!strcmp( ina233_cmd_list[idx].cmd_str , (char*) pData)){
				int base = 10;
				bData = (const char*) ina233_read_buffer (ina233_cmd_list[idx].reg, (uint8_t*) &bRead, ina233_cmd_list[idx].nBytes_rd);
				if(bData){
					if(reshex)
						base = 16;
					itoa(bRead, w, base);
					if(base == 16){
						if(!strcmp(ina233_cmd_list[idx].resp, rSTR))
							snprintf(resp_buffer, 30, rHexSTR, w);
					}
					else
						snprintf(resp_buffer, 30, ina233_cmd_list[idx].resp, w);
					bData = (const char *) resp_buffer;
				}
				FExit = 1;
			}
			break;
		case CMD_T_GET_FLOAT:
			if(!strcmp( ina233_cmd_list[idx].cmd_str , (char*) pData)){
				bData = (const char*) ina233_read_buffer (ina233_cmd_list[idx].reg, (uint8_t*) &bRead, ina233_cmd_list[idx].nBytes_rd);
				if(bData){
					itoa(bRead, w, 10);
					const char* t = NULL;
					T_PARSE_FUNC_CALC calc_func = (T_PARSE_FUNC_CALC) ina233_cmd_list[idx].myFunc;
					if(calc_func){
						float d = calc_func(bRead);
						t = float2str(d);
					}
					else{
						t = float2str(bRead);
					}
					snprintf(resp_buffer, 30, ina233_cmd_list[idx].resp, t);
					bData = (const char*) resp_buffer;
				}
				FExit = 1;
			}
			break;
		case CMD_T_SET_FLOAT:
				index = find((char*) pData ,'=', 0);
				if(index == -1)
					break;
				if(!strncmp(ina233_cmd_list[idx].cmd_str , (char*) pData, index)){
					bData = (const char*) pData+(index+1);
					wdata = stof(bData);
					T_PARSE_FUNC_CALC calc_func = (T_PARSE_FUNC_CALC) ina233_cmd_list[idx].myFunc;
					if(calc_func){
						wdata = calc_func(wdata);
					}
					bWrite = trunc(wdata);
					if(ina233_write_buffer(ina233_cmd_list[idx].reg, (uint8_t*) &bWrite, ina233_cmd_list[idx].nBytes_wr))
						bData = rOK;
					else
						bData = rERROR;
					FExit = 1;
				}
			break;
		case CMD_T_SET:
			index = find((char*) pData ,'=', 0);
			if(index == -1)
				break;
			if(!strncmp(ina233_cmd_list[idx].cmd_str , (char*) pData, index)){
				bData = (const char*) pData+(index+1);
				bWrite = atoi(bData);
				if(ina233_write_buffer(ina233_cmd_list[idx].reg, (uint8_t*) &bWrite, ina233_cmd_list[idx].nBytes_wr))
					bData = rOK;
				else
					bData = rERROR;
				FExit = 1;
			}
			break;
		case CMD_T_INFO:
			index = find((char*) pData ,'=', 0);
			if(index == -1)
				break;
			if(!strncmp( ina233_cmd_list[idx].cmd_str , (char*) pData, index)){
				if(ina233_cmd_list[idx].myFunc)
					bData = (const char*) ina233_cmd_list[idx].myFunc(idx, pData, bSize);
				else
					bData = ina233_cmd_list[idx].resp;
				FExit = 1;
			}
			break;
		}
		idx++;
	}
	if(FExit == 0){
		bData = rERROR;
	}
	return bData;
}

const uint8_t* add_byte (uint8_t byte)  //Funci�n que a�ade en el buffer los bytes que llegan.	Esta funci�n se invoca en el freeRtos para enviarlo ya por UART
{
	const uint8_t* pData = NULL;

	/* check if buffer overflow */
	if((count_data+1) > (sizeof(com_buffer)-1)){
		count_data = 0;
		return NULL;
	}

	switch(byte){
		case '\r':			//Cuando detecta el \r a�ade al buffer un \0 para convertirlo en una cadena de texto y luego envia estos valores a "Parse Command" donde se pasa el nombre del comando con su tama�o.
			/* parse command */							//Para que as� coja el valor
			com_buffer[count_data++] ='\0';
			pData = parse_command(com_buffer, count_data);
			count_data = 0;
			return pData;
		case '\n':
			/* ignore */
			break;
		case '\b':
			if(count_data > 0){
				count_data--;
				/* __sync_uart_write(rBAKSPACE , strlen(rBAKSPACE), 100); */
			}
			break;
			/* JM CHANGES */
		case '\177':					//El valor que em dona el backspace ja que el '\b' no el pil
			//case '\b \b':
			/* ---------- */
			if(count_data > 0){
				count_data--;
				/* __sync_uart_write(rBAKSPACE , strlen(rBAKSPACE), 100); */
			}
			break;
		default:
			if(isalnum(byte) || ispunct(byte))
				com_buffer[count_data++] = byte;
			break;
	}
	return NULL;
}


#endif

void OnIna233_response(const char* response)
{
	send_uart_message((uint8_t*) response, strlen(response));
}

void OnIna233_Ok (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	OnIna233_response("OK\n");
}

void OnIna233_Error (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
	OnIna233_response("ERROR\n");
}

void AT_Received(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize)
{
	OnIna233_response("OK\n");
}

bool FInitFailed = false;
bool FInitComplete = false;
uint16_t timer_cnt = 0;

void OnInit(uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	if(pData[0] == CMD_GET_VIN_OV_W_LIM){
		FInitComplete = true;
	}
}

void OnInitFailure(uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
	FInitFailed = true;
}

/* Send new command to ina233 using i2c */
void ina233_send_command (enum INA233_COMMANDS_REG reg, OnI2cMessageComplete evMC, OnI2cMessageError evME)
{
	i2c_pushMessage(0x80, (uint8_t*) &reg, sizeof(uint8_t), 0, 0, evMC, evME);
}

/* read buffer from ina233 */
void ina233_read_buffer (enum INA233_COMMANDS_REG reg, uint8_t pSize, OnI2cMessageComplete evMC, OnI2cMessageError evME)
{
	i2c_pushMessage(0x81, (uint8_t*) &reg, sizeof(uint8_t), pSize, 0, evMC, evME);
}

/* write buffer from ina233 */
void ina233_write_buffer (enum INA233_COMMANDS_REG reg, uint8_t *pData, uint8_t pSize, OnI2cMessageComplete evMC, OnI2cMessageError evME)
{
	uint8_t buffer[pSize + 1];
	buffer[0] = reg;
	memcpy(buffer + 1, pData, pSize);
	i2c_pushMessage(0x80, buffer, pSize + 1, 0, 0, evMC, evME);
}

void configure_ina233(void)
{
	// Clear all previous faults
	ina233_send_command(CMD_CLEAR_FAULTS, OnInit, OnInitFailure);

    /* Set CONFIGURATION ADC to 64 samples; Ts = 2ms; Continuous Mode */
    /* AT+ADC=18287 [0x476f] -> 0x76f */
	// ina233_write_word(CMD_GET_MFR_ADC_CONFIG , ina233_adc_config);
	ina233_write_buffer(CMD_GET_MFR_ADC_CONFIG, (uint8_t*) &ina233_adc_config, sizeof(ina233_adc_config), OnInit, OnInitFailure);

	/* Set CALIBRATION to m = 16384 and CLSB to 61 uA */
    /* AT+CAL=4194 */
	// ina233_write_word(CMD_GET_MFR_CALIBRATION, ina233_calibration);
	ina233_write_buffer(CMD_GET_MFR_CALIBRATION, (uint8_t*) &ina233_calibration, sizeof(ina233_calibration), OnInit, OnInitFailure);

	/* Set OVERCURRENT alert limit to 1'5 A -> 24576 */
	//Write --> Y = 16384 x X
	/* AT+IOUT_W_LIM?*/ /*16384 [0x4000]*/
	//OVERCURRENT
	// ina233_write_word(0x4A,ina233_iout_oc_w_lim);
	ina233_write_buffer(CMD_GET_IOUT_W_LIMIT, (uint8_t*) &ina233_iout_oc_w_lim, sizeof(ina233_iout_oc_w_lim), OnInit, OnInitFailure);

	//UNDERVOLTAGE
	/* LSB = 1.25mV -> X = 800; 800X3 = 2400 -> 3V  */
	// ina233_write_word(0x58,ina233_vin_uv_w_lim);
	ina233_write_buffer(CMD_GET_VIN_UV_W_LIM, (uint8_t*) &ina233_vin_uv_w_lim, sizeof(ina233_vin_uv_w_lim),OnInit, OnInitFailure);

	//Output OVERPOWER warn limit threshold
	//AT+PIN_OP_W_LIM?
	// ina233_write_word(0x6B,ina233_pout_op_w_lim);
	ina233_write_buffer(CMD_GET_PIN_OP_W_LIM, (uint8_t*) &ina233_pout_op_w_lim, sizeof(ina233_pout_op_w_lim), OnInit, OnInitFailure);

	//OVERVOLTAGE
	/* LSB = 1.25mV -> X = 800; */
	// ina233_write_word(0x57,ina233_vin_ov_w_lim);
	ina233_write_buffer(CMD_GET_VIN_OV_W_LIM, (uint8_t*) &ina233_vin_ov_w_lim, sizeof(ina233_vin_ov_w_lim), OnInit, OnInitFailure);

}

void parseUARTCommand (uint8_t *pData, uint16_t pSize)
{
	int FExit = 0;			// Flag to End parse
	uint16_t index;
	uint16_t idx = 0;	// locate command parse

	// Convert to Upper Case
	pData = toupper_str(pData);

	while((ina233_cmd_list[idx].reg != 0) &&( FExit == 0)){
		switch(ina233_cmd_list[idx].c_type){
		case CMD_T_COMMAND:
			if(!strcmp( ina233_cmd_list[idx].cmd_str , (char*) pData)){
				if(ina233_cmd_list[idx].myFunc){
					ina233_cmd_list[idx].myFunc(&ina233_cmd_list[idx], pData, pSize);
				}
				else
					ina233_send_command(ina233_cmd_list[idx].reg, OnIna233_Ok, OnIna233_Error);
				FExit = 1;
			}
			break;
		case CMD_T_GET:
			if(!strcmp( ina233_cmd_list[idx].cmd_str , (char*) pData)){
				if(ina233_cmd_list[idx].myFunc){
					ina233_cmd_list[idx].myFunc(&ina233_cmd_list[idx], pData, pSize);
				}
				else
					OnIna233_response("ERROR\n");
				FExit = 1;
			}
			break;
		case CMD_T_GET_FLOAT:
			if(!strcmp( ina233_cmd_list[idx].cmd_str , (char*) pData)){
				if(ina233_cmd_list[idx].myFunc){
					ina233_cmd_list[idx].myFunc(&ina233_cmd_list[idx], pData, pSize);
				}
				else
					OnIna233_response("ERROR\n");
				FExit = 1;
			}
			break;
		case CMD_T_SET_FLOAT:
				index = find((char*) pData ,'=', 0);
				if(index == -1)
					break;
				if(!strncmp(ina233_cmd_list[idx].cmd_str , (char*) pData, index)){
					FExit = 1;
				}
			break;
		case CMD_T_SET:
			index = find((char*) pData ,'=', 0);
			if(index == -1)
				break;
			if(!strncmp(ina233_cmd_list[idx].cmd_str , (char*) pData, index)){
				FExit = 1;
			}
			break;
		case CMD_T_INFO:
			index = find((char*) pData ,'=', 0);
			if(index == -1)
				break;
			if(!strncmp( ina233_cmd_list[idx].cmd_str , (char*) pData, index)){
				FExit = 1;
			}
			break;
		}
		idx++;
	}
	if(FExit == 0){
		OnIna233_response("ERROR\n");
	}
}

float m_voltage = 0.0;
float m_current = 0.0;
float m_power = 0.0;
uint8_t status = 0;

void OnVoltageRead (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	uint16_t* value = (uint16_t*) pRespData;
	m_voltage = cv_r * (float) value[0];
	update_voltage(m_voltage);
}

void OnVoltageReadFailure (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
}

void OnCurrentRead (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	uint16_t* value = (uint16_t*) pRespData;
	m_current = (float) value[0] / ci_r;
	update_current(m_current);
}

void OnCurrentReadFailure (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
}

void OnPowerRead (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	// uint16_t* value = (uint16_t*) pRespData;
	// m_power = (float) value[0] * cv_r;
	m_power = m_current * m_voltage;
	update_power(m_power);
}

void OnPowerReadFailure (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
}

void get_voltage(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize)
{
	char data[16];
	sprintf(data, "%s V\n", float2str(m_voltage));
	OnIna233_response(data);
}

void get_current(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize)
{
	char data[16];
	sprintf(data, "%s A\n", float2str(m_current));
	OnIna233_response(data);
}

void get_power(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize)
{
	char data[16];
	sprintf(data, "%s W\n", float2str(m_power));
	OnIna233_response(data);
}

void OnStatusRead (uint16_t addr, uint8_t *pData, uint16_t pSize, uint8_t *pRespData, uint16_t pRespSize, uint16_t delay)
{
	status = pRespData[0];
	update_status(status);
}

void OnStatusReadFailure (uint32_t errorCode, uint16_t addr, uint8_t *pData, uint16_t pSize)
{
}

void get_status(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize)
{
	char data[16];
	sprintf(data, "Status: 0x%x\n", status);
	OnIna233_response(data);
}

void clear_status(struct t_INA233_COMMANDS *cmd, uint8_t *pData, uint16_t pSize)
{
	char i = 0xFF;
	ina233_write_buffer(CMD_GET_STATUS_INPUT, &i, 1, OnIna233_Ok, OnIna233_Error);
}


void ina233_schedule (void)
{
	struct t_uart_data msg;
	switch(ina233_state){
		case ina233INIT:
			FInitFailed = false;
			FInitComplete = false;
			configure_ina233();
			ina233_state = ina233_WAIT_INIT_COMPLETE;
			break;
		case ina233_WAIT_INIT_COMPLETE:
			if(FInitFailed){
				__NOP();
			}else if(FInitComplete){
				ina233_state = ina233_WAIT4COMMAND;
				enable_ina233_timer(250);
			}
			break;
		case ina233_WAIT4COMMAND:
			if(get_uart_message(&msg)){
				disable_ina233_timer();
				parseUARTCommand(msg.pData, msg.pSize);
				drop_uart_message();
				enable_ina233_timer(250);
			}
			else{
				if(isTimerExpired()){
					ina233_state = ina233_Status;
				}
			}
			break;
		case ina233_Status:
			ina233_read_buffer(CMD_GET_STATUS_INPUT, 1, OnStatusRead, OnStatusReadFailure);
			ina233_state = ina233_Measure_Voltage;
			break;
		case ina233_Measure_Voltage:
			// void ina233_read_buffer (enum INA233_COMMANDS_REG reg, uint8_t pSize, OnI2cMessageComplete evMC, OnI2cMessageError evME)
			ina233_read_buffer(CMD_GET_READ_VIN, 2, OnVoltageRead, OnVoltageReadFailure);
			ina233_state = ina233_Measure_Power;
			break;
		case ina233_Measure_Power:
			ina233_read_buffer(CMD_GET_READ_PIN, 2, OnPowerRead, OnPowerReadFailure);
			ina233_state = ina233_Measure_Current;
			break;
		case ina233_Measure_Current:
			ina233_read_buffer(CMD_GET_READ_IN, 2, OnCurrentRead, OnCurrentReadFailure);
			ina233_state = ina233_WAIT4COMMAND;
			enable_ina233_timer(250);
			break;
	}
}

bool isTimerExpired (void)
{
	return timer_cnt == 0;
}

void enable_ina233_timer(uint16_t ms_interval)
{
	timer_cnt = ms_interval;
	__HAL_TIM_SET_COUNTER(&htim6, 0);
	__HAL_TIM_CLEAR_IT(&htim6 ,TIM_IT_UPDATE);
	HAL_TIM_Base_Start_IT(&htim6);
}

void disable_ina233_timer(void)
{
	HAL_TIM_Base_Stop_IT(&htim6);
}


void ina233_timer(void)
{
	if(timer_cnt > 0){
		timer_cnt--;
		if(timer_cnt == 0)
			disable_ina233_timer();
	}
}

