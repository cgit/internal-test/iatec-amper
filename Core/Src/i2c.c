/*
 * test.c
 *
 *  Created on: Jan 8, 2021
 *      Author: isee
 */

#include <i2c.h>
#include <stdlib.h>
#include <string.h>
#include <main.h>

#include "queue.h"

Queue_t q_i2cMessages;

enum t_i2c_state {
	i2cINIT,
	i2cWAITMessage,
	i2cWAIT_TxComplete,
	i2cWAIT_RxComplete,
	i2cMessage_Complete,
	i2cErrorMessage,
	i2cMessageRestart
};

enum t_i2c_state i2c_state = i2cINIT;

extern I2C_HandleTypeDef hi2c1;

#define qi2cMessageSize		50
#define i2cMESSAGE_RETRIES	3
#define i2cADDRetries		3

bool i2cWaitTime = false;
int i2cCountTime = 0;
bool i2cTimeout = false;

void free_message(struct t_i2c_msg* i);
void free_message_data(struct t_i2c_msg* i);
void i2c_setTimeoutTimer (uint16_t timeout);

void i2c_initqueues(void)
{
	q_init(&q_i2cMessages, sizeof(struct t_i2c_msg), qi2cMessageSize, FIFO, false);
}

bool i2c_pushMessage (uint16_t addr, uint8_t *pData, uint16_t pSize, uint16_t wRespSize, uint16_t smDelay, OnI2cMessageComplete evMC, OnI2cMessageError evME)
{
	struct t_i2c_msg i;
	i.addr = addr;
	if(pSize > 0)
		i.pData = malloc(pSize);
	else
		i.pData = NULL;
	memcpy(i.pData, pData, pSize);
	i.pSize = pSize;
	if(wRespSize > 0){
		i.pRespData = malloc(wRespSize);
		memset(i.pRespData, 0, wRespSize);
	}
	else
		i.pRespData = NULL;
	i.pRespSize = wRespSize;
	i.smDelay = smDelay;
	i.evMessageComplete = evMC;
	i.evMessageError = evME;
	if(!q_push(&q_i2cMessages, &i)){
		free_message_data(&i);
		return false;
	}
	return true;
}

bool i2c_getMessage (struct t_i2c_msg *message)
{
	return q_peek(&q_i2cMessages, message);
}

bool i2c_dropMessage()
{
	struct t_i2c_msg t;
	bool res = q_pop(&q_i2cMessages, &t);
	free_message_data(&t);
	return res;
}

void free_message_data(struct t_i2c_msg* i)
{
	if(!i)
		return;
	if(i->pData)
		free(i->pData);
	if(i->pRespData)
		free(i->pRespData);
}

void free_message(struct t_i2c_msg* i)
{
	if(!i)
		return;
	free_message_data(i);
	free(i);
}

bool i2c_TX_Message(struct t_i2c_msg *msg)
{
	int ret = i2cADDRetries;
	do{
		if(HAL_I2C_Master_Transmit_DMA(&hi2c1, msg->addr, msg->pData , msg->pSize) != HAL_OK)
		{
		  // Error_Handler();
			return false;
		}
		while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY) {}
		ret--;
		if(ret <= 0)
			return false;
	}while(HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF);
	return true;
}

bool i2c_RX_Message(struct t_i2c_msg *msg)
{
	uint8_t ret = i2cADDRetries;
	do
	{
	    if(HAL_I2C_Master_Receive_DMA(&hi2c1, msg->addr, msg->pRespData, msg->pRespSize) != HAL_OK)
	    {
	      /* Error_Handler() function is called when error occurs. */
	      // Error_Handler();
	    	return false;
	    }

	    /*##-5- Wait for the end of the transfer #################################*/
	    /*  Before starting a new communication transfer, you need to check the current
	        state of the peripheral; if it’s busy you need to wait for the end of current
	        transfer before starting a new one.
	        For simplicity reasons, this example is just waiting till the end of the
	        transfer, but application may perform other tasks while transfer operation
	        is ongoing. */
	    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	    {
	    }

	    /* When Acknowledge failure occurs (Slave don't acknowledge it's address)
	       Master restarts communication */
	    ret--;
	    if(!ret)
	    	return false;
	  }
	  while(HAL_I2C_GetError(&hi2c1) == HAL_I2C_ERROR_AF);
	return true;
}

bool i2cTx_Complete = false;
bool i2cRx_Complete = false;

void i2c_SchedMessage (void)
{
	struct t_i2c_msg msg;
	bool result = false;
	switch(i2c_state){
		case i2cINIT:
			i2c_initqueues();
			i2c_state = i2cWAITMessage;
			break;
		case i2cWAITMessage:
			if(i2c_getMessage(&msg)){
				i2cTx_Complete = false;
				result = i2c_TX_Message(&msg);
				if(result){
					i2c_state = i2cWAIT_TxComplete;
					i2c_setTimeoutTimer(10);
				}else{
					i2c_state = i2cErrorMessage;
				}
			}
			break;
		case i2cWAIT_TxComplete:
			if(i2cTx_Complete){
				if(i2c_getMessage(&msg)){
					if(msg.pRespSize > 0){
						i2cRx_Complete = false;
						i2c_state = i2cWAIT_RxComplete;
						result = i2c_RX_Message(&msg);
						if(!result){
							i2c_state = i2cErrorMessage;
						}
						else{
							i2c_setTimeoutTimer(10);
						}
					}
					else{
						i2c_state = i2cMessage_Complete;
					}
				}
			}
			else{
				if(i2cTimeout){
					i2c_state = i2cErrorMessage;
				}
			}
			break;
		case i2cWAIT_RxComplete:
			if(i2cRx_Complete){
				i2c_state = i2cMessage_Complete;
			}
			else{
				if(i2cTimeout){
					i2c_state = i2cErrorMessage;
				}
			}
			break;
		case i2cMessage_Complete:
			i2c_getMessage(&msg);
			if(msg.evMessageComplete)
				msg.evMessageComplete(msg.addr, msg.pData, msg.pSize, msg.pRespData, msg.pRespSize, msg.smDelay);
			i2c_dropMessage();
			i2c_state = i2cMessageRestart;
			break;
		case i2cErrorMessage:
			i2c_getMessage(&msg);
			if(msg.evMessageError)
				msg.evMessageError(HAL_I2C_GetError(&hi2c1), msg.addr, msg.pData, msg.pSize);
			i2c_dropMessage();
			i2c_state = i2cMessageRestart;
			break;
		case i2cMessageRestart:
			i2c_state = i2cWAITMessage;
			break;
	}
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	i2cTx_Complete = true;
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	i2cRx_Complete = true;
}

void i2c_setTimeoutTimer (uint16_t timeout)
{
	i2cTimeout = false;
	i2cCountTime = timeout;
	i2cWaitTime = true;
}

void i2c_Timer(void)
{
	if(i2cWaitTime){
		i2cWaitTime = false;
		i2cCountTime--;
		if(!i2cCountTime){
			i2cTimeout = true;
		}
		else{
			i2cWaitTime = true;
		}
	}
}



